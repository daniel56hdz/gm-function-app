const moment = require('moment');
const {
  checkBlobExists,
  createBlobService,
  createReadStream,
  generateThumbnailBuffer,
  uploadToBlobContainer
} = require('../services');

module.exports = function(context, { body, query }) {
  context.log('Javascript HTTP trigger function processed a request.');
  const { accesskey, thumbnailscontainer, borderwidth, thumbwidth, bordercolor } = query;
  // Requires access key and thumbnailscontainer
  if (!(accesskey && thumbnailscontainer)) {
    const reqQueryParams = { accesskey, thumbnailscontainer };
    const message = 'missing required keys';
    const keys = Object.keys(reqQueryParams).filter(k => !reqQueryParams[k]);
    context.log.error(`${message}: ${keys}`);
    context.res = { status: 400, body: { message, keys } };
    context.done();
  } else if (
    body &&
    body.length > 0 &&
    body[0].eventType === 'Microsoft.EventGrid.SubscriptionValidationEvent'
  ) {
    // If the request is for subscription validation, send back the validation code.
    context.log('Validate request receieved');
    context.res = { body: { ValidationResponse: body[0].data.validationCode } };
    context.done();
  } else if (body) {
    // The request is not for subscription validation, so it's for one or more events.
    for (var i = 0; i < body.length; i++) {
      // Handle one event.
      const options = { borderwidth, thumbwidth, bordercolor };
      const { eventTime, data } = body[i];
      const [, AZURE_HOST, AZURE_STORAGE_ACC, blobContainer, blobName] = data.url.match(
        new RegExp('(.*?//(.*?).blob.core.windows.net/)/?([^/.]*)/(.*)')
      );
      const thumbName = `${blobContainer}/${blobName}.jpg`;
      context.log(`Time: ${eventTime}`);
      context.log(`AZURE_HOST: ${AZURE_HOST}`);
      context.log(`AZURE_STORAGE_ACC: ${AZURE_STORAGE_ACC}`);
      context.log(`Container: ${blobContainer}`);
      context.log(`Blobname: ${blobName}`);
      context.log(`options: ${JSON.stringify(options, (k, v) => (v === undefined ? null : v))}`);
      if (blobContainer === thumbnailscontainer) {
        return context.log.error(
          `Requests from thumbnails container, '${thumbnailscontainer}', are not processed`
        );
      }
      const blobService = createBlobService(
        AZURE_STORAGE_ACC,
        accesskey.trim().replace(/\s/g, '+'),
        AZURE_HOST
      );
      checkBlobExists(blobService, blobContainer, blobName)
        .then(() => createReadStream(blobService, blobContainer, blobName))
        .then(str => generateThumbnailBuffer(context, options, str, blobName))
        .then(optimizedBuffer =>
          uploadToBlobContainer(blobService, thumbnailscontainer, optimizedBuffer, thumbName)
        )
        .catch(err => {
          context.log.error('Error encountered', err);
        })
        .finally(() => {
          context.log.info(`Completed ${moment().format()}`);
          context.done();
        });
    }
  } else {
    context.log('A body must be sent');
    context.done();
  }
};
