# lja-thumbnail-functions

Azure functions app for generating thumbnails for LJA Blob Storage Accounts using blob storage to upload images/pdfs.

## Package dependencies

Use `npm install` instead of yarn. NPM is used for better integration with the Azure Functions cloud environment.

## Environment variables

| Variable                                 | Description |
| ---------------------------------------- | ----------- |
| AzureWebJobsDashboard                    | Optional. Storage account connection string for storing logs and displaying them in the Monitor tab in the portal.|
| AzureWebJobsStorage                      | The Azure Functions runtime uses this storage account connection string for all functions except for HTTP triggered functions. |
| BORDER_COLOR                             | Optional. Border color of thumbnail. Border width defaults to 10 if BORDER_WIDTH not specified. |
| BORDER_WIDTH                             | Optional. Width of thumbnail border. |
| FUNCTIONS_EXTENSION_VERSION              | The version of the Azure Functions runtime to use in this function app. A tilde with major version means use the latest version of that major version (for example, "~1"). |
| NODE_ENV                                 | When set to production, allows us to pass in secrets and other runtime configurations. |
| THUMB_WIDTH                              | Width of thumbnail. |
| THUMBNAILS_CONTAINER                     | Blob Container that will hold all generated thumnails. |
| WEBSITE_CONTENTAZUREFILECONNECTIONSTRING | For consumption plans only. Connection string for storage account where the function app code and configuration are stored. |
| WEBSITE_CONTENTSHARE                     | For consumption plans only. The file path to the function app code and configuration. Used with WEBSITE_CONTENTAZUREFILECONNECTIONSTRING. Default is a unique string that begins with the function app name. |
| WEBSITE_NODE_DEFAULT_VERSION             | Default is "6.5.0". |

## Adding event grid subscription for a blob storage account

1. Add the storage account access key to the `Application Settings` in azure portal.
2. Modify access keys enum in `services/azure.service.js`.
3. Add Event Subscription in azure portal.