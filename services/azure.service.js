const azure = require('azure-storage');
const Promise = require('bluebird');
const stream = require('stream');

const { buildResponse } = require('../utils');

function createBlobService(azureStorageAcc, azureAccessKey, azureHost) {
  return azure.createBlobService(azureStorageAcc, azureAccessKey, azureHost);
}

exports.createBlobService = createBlobService;

function checkBlobExists(blobService, blobContainer, blobName) {
  return new Promise((resolve, reject) => {
    blobService.doesBlobExist(blobContainer, blobName, (err, result) => {
      if (err) {
        reject(buildResponse(200, 'Could not process request.', err));
      }
      if (!result.exists) {
        reject(buildResponse(200, `Could not locate "${blobName}".`));
      }
      resolve(null);
    });
  });
}
exports.checkBlobExists = checkBlobExists;

function createReadStream(blobService, blobContainer, blobName) {
  return new Promise((resolve, reject) => {
    const str = blobService.createReadStream(blobContainer, blobName, err => {
      if (err) {
        reject(buildResponse(200, `Could not create read stream for "${blobName}".`, err));
      }
    });
    resolve(str);
  });
}

exports.createReadStream = createReadStream;

function uploadToBlobContainer(blobService, thumbnailsContainer, buffer, thumbName) {
  return new Promise((resolve, reject) => {
    const str = new stream.PassThrough();
    str.end(buffer);
    const headers = { contentSettings: { contentType: 'image/jpeg' } };
    str.pipe(
      blobService.createWriteStreamToBlockBlob(thumbnailsContainer, thumbName, headers, err => {
        if (err) {
          reject(err);
        }
        resolve();
      })
    );
  });
}

exports.uploadToBlobContainer = uploadToBlobContainer;
