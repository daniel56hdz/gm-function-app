const azureService = require('./azure.service');
exports.createBlobService = azureService.createBlobService;
exports.checkBlobExists = azureService.checkBlobExists;
exports.createReadStream = azureService.createReadStream;
exports.uploadToBlobContainer = azureService.uploadToBlobContainer;
const gmService = require('./gm.service');
exports.generateThumbnailBuffer = gmService.generateThumbnailBuffer;
