const Promise = require('bluebird');
const path = require('path');
const { buildResponse } = require('../utils');
const gm = require('gm').subClass({ appPath: path.win32.join(__dirname, '../gm/') });

const DEFAULT_BORDER_COLOR = 'black';
const DEFAULT_THUMB_WIDTH = 250;

/** Use read stream to resize blob and create new buffer.  */
function generateThumbnailBuffer(context, options, str, blobName) {
  let borderwidth = +options.borderwidth;

  return new Promise((resolve, reject) => {
    gm(str).format((err, isSupported) => {
      if (err || !isSupported) {
        const msg = `Cannot create thumbnail for "${blobName}" because format is unsupported.`;
        reject(buildResponse(200, msg, err));
      }
    });

    // NOTE. order of operations does matter for the image result and size
    const thmb = gm(str)
      .colorspace('Transparent')
      .setFormat('jpeg')
      .resize(+options.thumbwidth || DEFAULT_THUMB_WIDTH)
      .strip()
      .dither(false)
      .compress('JPEG')
      .interlace('Line')
      .samplingFactor(2, 1)
      .quality(65)
      .gaussian(0.01);

    // NOTE. using old implementation of BORDER_COLOR for legacy support, REMOVE AT 2.X
    if (options.bordercolor) {
      context.log.warn(
        '`options.bordercolor` will no longer be supported at version 2.x, use query parameters `borderwidth` and `bordercolor` instead.'
      );
      borderwidth = borderwidth || 10; // legacy static width
    }
    if (borderwidth) {
      thmb
        .borderColor(options.bordercolor || DEFAULT_BORDER_COLOR)
        .border(borderwidth, borderwidth);
    }
    thmb.toBuffer((err, buffer) => {
      if (err) {
        reject(buildResponse(200, `Could not resize ${blobName} to thumbnail.`, err));
      }
      resolve(buffer);
    });
  });
}

exports.generateThumbnailBuffer = generateThumbnailBuffer;
